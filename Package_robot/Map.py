#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""


from math import floor
from random import randint, sample


class Grid:
    """Classe grille"""

    def __init__(self, hauteur, largeur, nbre_robots):
        """initialisation de la grille et placement du robot"""
        taille_max = 4000
        self.hauteur = int(hauteur)
        self.largeur = int(largeur)
        self.taille = self.hauteur * self.largeur
        # on fixe le nombre max de robots
        max_robots = min(hauteur, largeur) - 1
        if min(hauteur, largeur) == 0:
            nbre_robots = 0
        if nbre_robots > max_robots:
            nbre_robots = max_robots
            print("Trop de robots, " + str(max_robots) + " robots générés")
        try:
            self.position_x = sample(range(hauteur), nbre_robots)
            self.position_y = sample(range(largeur), nbre_robots)
        except:
            self.position_x = []
            self.position_y = []
        self.grid = []
        if (
            (self.hauteur > 0)
            and (self.largeur > 0)
            and (self.hauteur <= taille_max)
            and (self.largeur <= taille_max)
        ):  # test si les dimensions de la grille ne sont pas négatives,
            # et ne sont pas trop grandes
            for i in range(self.hauteur):
                self.grid.append([])
                for j in range(self.largeur):
                    self.grid[i].append(0)
            for i in range(len(self.position_x)):
                self.grid[self.position_x[i]][self.position_y[i]] = i + 1
            # nomrbre aleatoire d'obstacles (deplacable ou non)
            rand = randint(floor(self.taille / 15), floor(self.taille / 10))
            self.init_obstacle(rand)
            self.init_obstacle_deplacable(rand)
        else:
            print("Erreur : taille de grille")

    def init_obstacle(self, nbre_obstacle):
        """placement aleatoire des obstacles sur la grille"""
        for i in range(nbre_obstacle):
            randx = self.position_x[0]
            randy = self.position_y[0]
            # on place les obstacles la ou les robots ne sont pas
            while self.grid[randx][randy] != 0:
                randx = randint(0, self.hauteur - 1)
                randy = randint(0, self.largeur - 1)
            self.grid[randx][randy] = "X"

    def init_obstacle_deplacable(self, nbre_obstacle_dep):
        """placement aleatoire des obstacles deplacables sur la grille"""
        for i in range(nbre_obstacle_dep):
            randx = self.position_x[0]
            randy = self.position_y[0]
            # on place les obstacles la ou les robots et les obstacles
            # ne sont pas
            while self.grid[randx][randy] != 0:
                randx = randint(0, self.hauteur - 1)
                randy = randint(0, self.largeur - 1)
            self.grid[randx][randy] = "°"

    def print(self):
        """affichage de la grille"""
        for i in range(self.hauteur):
            affichage = "| "
            for j in range(self.largeur):
                affichage += str(self.grid[i][j]) + " "
            print(affichage + "|")
        print("")

    def up(self, id_robot):
        """deplacement du robot vers le haut"""
        id_robot -= 1  # le numéro du robot correspond à son identifiant +1
        if self.position_x[id_robot] == 0:
            return False
        elif self.grid[self.position_x[id_robot] - 1][self.position_y[id_robot]] == "X":
            return False
        elif self.grid[self.position_x[id_robot] - 1][self.position_y[id_robot]] == "°":
            if self.position_x[id_robot] == 0:
                return False
            elif (
                self.grid[self.position_x[id_robot] - 2][self.position_y[id_robot]] != 0
            ):
                return False
            else:
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
                self.position_x[id_robot] = self.position_x[id_robot] - 1
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                    id_robot + 1
                )
                self.grid[self.position_x[id_robot] - 1][
                    self.position_y[id_robot]
                ] = "°"
                return True
        elif self.grid[self.position_x[id_robot] - 1][self.position_y[id_robot]] == 0:
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
            self.position_x[id_robot] = self.position_x[id_robot] - 1
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                id_robot + 1
            )
            return True
        else:
            return False

    def down(self, id_robot):
        """deplacement du robot vers le bas"""
        id_robot -= 1  # le numéro du robot correspond à son identifiant +1
        if self.position_x[id_robot] == self.hauteur - 1:
            return False
        elif self.grid[self.position_x[id_robot] + 1][self.position_y[id_robot]] == "X":
            return False
        elif self.grid[self.position_x[id_robot] + 1][self.position_y[id_robot]] == "°":
            if self.position_x[id_robot] == self.hauteur - 1:
                return False
            elif (
                self.grid[self.position_x[id_robot] + 2][self.position_y[id_robot]] != 0
            ):
                return False
            else:
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
                self.position_x[id_robot] = self.position_x[id_robot] + 1
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                    id_robot + 1
                )
                self.grid[self.position_x[id_robot] + 1][
                    self.position_y[id_robot]
                ] = "°"
                return True
        elif self.grid[self.position_x[id_robot] + 1][self.position_y[id_robot]] == 0:
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
            self.position_x[id_robot] = self.position_x[id_robot] + 1
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                id_robot + 1
            )
            return True
        else:
            return False

    def left(self, id_robot):
        """deplacement du robot vers la gauche"""
        id_robot -= 1  # le numéro du robot correspond à son identifiant +1
        if self.position_y[id_robot] == 0:
            return False
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] - 1] == "X":
            return False
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] - 1] == "°":
            if self.position_y[id_robot] == 0:
                return False
            elif (
                self.grid[self.position_x[id_robot]][self.position_y[id_robot] - 2] != 0
            ):
                return False
            else:
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
                self.position_y[id_robot] = self.position_y[id_robot] - 1
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                    id_robot + 1
                )
                self.grid[self.position_x[id_robot]][
                    self.position_y[id_robot] - 1
                ] = "°"
                return True
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] - 1] == 0:
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
            self.position_y[id_robot] = self.position_y[id_robot] - 1
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                id_robot + 1
            )
            return True
        else:
            return False

    def right(self, id_robot):
        """deplacement du robot vers la droite"""
        id_robot -= 1  # le numéro du robot correspond à son identifiant +1
        if self.position_y[id_robot] == self.largeur - 1:
            return False
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] + 1] == "X":
            return False
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] + 1] == "°":
            if self.position_y[id_robot] == self.largeur - 1:
                return False
            elif (
                self.grid[self.position_x[id_robot]][self.position_y[id_robot] + 2] != 0
            ):
                return False
            else:
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
                self.position_y[id_robot] = self.position_y[id_robot] + 1
                self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                    id_robot + 1
                )
                self.grid[self.position_x[id_robot]][
                    self.position_y[id_robot] + 1
                ] = "°"
                return True
        elif self.grid[self.position_x[id_robot]][self.position_y[id_robot] + 1] == 0:
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = 0
            self.position_y[id_robot] = self.position_y[id_robot] + 1
            self.grid[self.position_x[id_robot]][self.position_y[id_robot]] = (
                id_robot + 1
            )
            return True
        else:
            return False
