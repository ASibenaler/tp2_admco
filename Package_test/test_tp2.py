#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""

import unittest

from Package_robot.Map import Grid


class Test_grille(unittest.TestCase):
    """Test la création des grilles"""

    def test_grille_ok(self):
        """Grille standard de taille 3x5"""
        self.grille = Grid(3, 5, 2)
        nbre_X = 0  # nombre d'obstacles sur la grille generee
        nbre_dep = 0  # nombre d'obstacles deplacables
        nbre_robots = 0  # nombre de robots sur la grille
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if self.grille.grid[i][j] == "X":
                    nbre_X += 1
                elif self.grille.grid[i][j] == "°":
                    nbre_dep += 1
                elif self.grille.grid[i][j] != 0:
                    nbre_robots += 1
        # il doit y avoir entre 1 et 2 obstacles pour une grille de taille 3*5
        # et 2 robots
        res = (nbre_X in [1, 2]) and (nbre_dep in [1, 2]) and (nbre_robots == 2)
        self.assertEqual(res, True)

    def test_grille_max_robots(self):
        """Grille standard de taille 3x5"""
        self.grille = Grid(3, 5, 20)
        nbre_robots = 0  # nombre de robots sur la grille
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (
                    (self.grille.grid[i][j] != "X")
                    and (self.grille.grid[i][j] != "°")
                    and (self.grille.grid[i][j] != 0)
                ):
                    nbre_robots += 1
        # il doit y avoir 3-1 robots
        res = nbre_robots == 2
        self.assertEqual(res, True)

    def test_grille_nulle(self):
        """Grille avec un paramètre nul"""
        self.grille = Grid(0, 3, 0)
        res = self.grille.grid
        self.assertEqual(res, [])

    def test_grille_grande(self):
        """Grille avec un paramètre trop grand"""
        self.grille = Grid(3, 5000, 1)
        res = self.grille.grid
        self.assertEqual(res, [])


class Test_deplacements(unittest.TestCase):
    """Test les déplacements du robot"""

    def setUp(self):
        """Executed before every test case"""
        # on genere une grille sans obstacles,
        # ou les robots ne sont pas pres d'un bord
        self.grille = Grid(6, 6, 2)
        while (
            (self.grille.hauteur - 1 in self.grille.position_x)
            or (self.grille.largeur - 1 in self.grille.position_y)
            or (0 in self.grille.position_x)
            or (0 in self.grille.position_y)
        ):
            self.grille = Grid(6, 6, 2)
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (self.grille.grid[i][j] == "X") or (self.grille.grid[i][j] == "°"):
                    self.grille.grid[i][j] = 0

    def test_down1(self):
        """Déplacement vers le bas, robot 1"""
        res = self.grille.down(1)
        self.assertEqual(res, True)

    def test_down2(self):
        """Déplacement vers le bas, robot 2"""
        res = self.grille.down(2)
        self.assertEqual(res, True)

    def test_up1(self):
        """Déplacement vers le haut, robot 1"""
        res = self.grille.up(1)
        self.assertEqual(res, True)

    def test_up2(self):
        """Déplacement vers le haut, robot 2"""
        res = self.grille.up(2)
        self.assertEqual(res, True)

    def test_right1(self):
        """Déplacement vers la droite, robot 1"""
        res = self.grille.right(1)
        self.assertEqual(res, True)

    def test_right2(self):
        """Déplacement vers la droite, robot 2"""
        res = self.grille.right(2)
        self.assertEqual(res, True)

    def test_left1(self):
        """Déplacement vers la gauche, robot 1"""
        res = self.grille.left(1)
        self.assertEqual(res, True)

    def test_left2(self):
        """Déplacement vers la gauche, robot 2"""
        res = self.grille.left(2)
        self.assertEqual(res, True)


class Test_collisions_bord(unittest.TestCase):
    """Test les collisions entre le robot et les bords de la grille"""

    def setUp(self):
        """Executed before every test case"""
        # on genere une grille sans obstacles avec un robot dans un coin
        self.grille = Grid(3, 3, 1)
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (self.grille.grid[i][j] == "X") or (self.grille.grid[i][j] == "°"):
                    self.grille.grid[i][j] = 0
        self.grille.position_x[0] = 0
        self.grille.position_y[0] = 0

    def test_collision_up(self):
        """Collision avec le bord haut"""
        res = self.grille.up(1)
        self.assertEqual(res, False)

    def test_collision_down(self):
        """Collision avec le bord bas"""
        self.grille.down(1)
        self.grille.down(1)
        res = self.grille.down(1)
        self.assertEqual(res, False)

    def test_collision_left(self):
        """Collision avec le bord gauche"""
        res = self.grille.left(1)
        self.assertEqual(res, False)

    def test_collision_right(self):
        """Collision avec le bord droit"""
        self.grille.right(1)
        self.grille.right(1)
        res = self.grille.right(1)
        self.assertEqual(res, False)


class Test_collisions_obstacle(unittest.TestCase):
    """Test les collisions entre le robot des obstacles"""

    def setUp(self):
        """Executed before every test case"""
        # on genere une grille sans obstacles, avec un robot au centre
        self.grille = Grid(3, 3, 1)
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (self.grille.grid[i][j] == "X") or (self.grille.grid[i][j] == "°"):
                    self.grille.grid[i][j] = 0
        self.grille.position_x[0] = 1
        self.grille.position_y[0] = 1
        self.grille.grid[1][
            0
        ] = "X"  # On place manuellement des obstacles tout autour du robot
        self.grille.grid[0][1] = "X"
        self.grille.grid[2][1] = "X"
        self.grille.grid[1][2] = "X"

    def test_collision_obs_up(self):
        """Collision avec un obstacle en haut"""
        res = self.grille.up(1)
        self.assertEqual(res, False)

    def test_collision_obs_down(self):
        """Collision avec un obstacle en bas"""
        res = self.grille.down(1)
        self.assertEqual(res, False)

    def test_collision_obs_left(self):
        """Collision avec un obstacle à gauche"""
        res = self.grille.left(1)
        self.assertEqual(res, False)

    def test_collision_obs_right(self):
        """Collision avec un obstacle à droite"""
        res = self.grille.right(1)
        self.assertEqual(res, False)


class Test_collisions_robot(unittest.TestCase):
    """Test les collisions entre les robots"""

    def setUp(self):
        """Executed before every test case"""
        # on genere une grille sans obstacles, avec un robot au centre
        self.grille = Grid(3, 3, 2)
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (self.grille.grid[i][j] == "X") or (self.grille.grid[i][j] == "°"):
                    self.grille.grid[i][j] = 0
        self.grille.position_x[0] = 1
        self.grille.position_y[0] = 1

    def test_collision_rob_up(self):
        """Collision avec un robot en haut"""
        # on place manuellement le 2eme robot au dessus du premier
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 0
        self.grille.position_x[1] = 0
        self.grille.position_y[1] = 1
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 2
        res = self.grille.up(1)
        self.assertEqual(res, False)

    def test_collision_rob_down(self):
        """Collision avec un robot en bas"""
        # on place manuellement le 2eme sous le premier
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 0
        self.grille.position_x[1] = 2
        self.grille.position_y[1] = 1
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 2
        res = self.grille.down(1)
        self.assertEqual(res, False)

    def test_collision_rob_left(self):
        """Collision avec un robot à gauche"""
        # on place manuellement le 2eme robot a gauche du premier
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 0
        self.grille.position_x[1] = 1
        self.grille.position_y[1] = 0
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 2
        res = self.grille.left(1)
        self.assertEqual(res, False)

    def test_collision_rob_right(self):
        """Collision avec un robot à droite"""
        # on place manuellement le 2eme robot a droite du premier
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 0
        self.grille.position_x[1] = 1
        self.grille.position_y[1] = 2
        self.grille.grid[self.grille.position_x[1]][self.grille.position_y[1]] = 2
        res = self.grille.right(1)
        self.assertEqual(res, False)


class Test_collisions_obstacle_deplacable(unittest.TestCase):
    """Test les collisions entre un robot et un obstacle deplacable"""

    def setUp(self):
        """Executed before every test case"""
        # on genere une grille sans obstacles, avec un robot au centre
        self.grille = Grid(5, 5, 1)
        for i in range(self.grille.hauteur):
            for j in range(self.grille.largeur):
                if (self.grille.grid[i][j] == "X") or (self.grille.grid[i][j] == "°"):
                    self.grille.grid[i][j] = 0
        self.grille.position_x[0] = 2
        self.grille.position_y[0] = 2

    def test_collision_obs_dep_up(self):
        """Test de collision avec un obstacle deplacable en haut"""
        # on place manuellement un obstacle deplacable au dessus du robot
        self.grille.grid[1][2] = "°"
        res = self.grille.up(1)
        self.assertEqual(res, True)

    def test_collision_obs_dep_up_err(self):
        """Test de collision avec un obsatcle deplacable en haut"""
        # on place manuellement un obstacle deplacable au dessus du robot
        self.grille.grid[1][2] = "°"
        self.grille.grid[0][2] = "X"
        res = self.grille.up(1)
        self.assertEqual(res, False)

    def test_collision_obs_dep_down(self):
        """Test de collision avec un obstacle deplacable en bas"""
        # on place manuellement un obstacle deplacable au dessous du robot
        self.grille.grid[3][2] = "°"
        res = self.grille.down(1)
        self.assertEqual(res, True)

    def test_collision_obs_dep_down_err(self):
        """Test de collision avec un obsatcle deplacable en bas"""
        # on place manuellement un obstacle deplacable au dessous du robot
        self.grille.grid[3][2] = "°"
        self.grille.grid[4][2] = "X"
        res = self.grille.down(1)
        self.assertEqual(res, False)

    def test_collision_obs_dep_left(self):
        """Test de collision avec un obstacle deplacable a gauche"""
        # on place manuellement un obstacle deplacable a gauche du robot
        self.grille.grid[2][1] = "°"
        res = self.grille.left(1)
        self.assertEqual(res, True)

    def test_collision_obs_dep_left_err(self):
        """Test de collision avec un obsatcle deplacable a gauche"""
        # on place manuellement un obstacle deplacable a gauche du robot
        self.grille.grid[2][1] = "°"
        self.grille.grid[2][0] = "X"
        res = self.grille.left(1)
        self.assertEqual(res, False)

    def test_collision_obs_dep_right(self):
        """Test de collision avec un obstacle deplacable a droite"""
        # on place manuellement un obstacle deplacable a droite du robot
        self.grille.grid[2][3] = "°"
        res = self.grille.right(1)
        self.assertEqual(res, True)

    def test_collision_obs_dep_right_err(self):
        """Test de collision avec un obsatcle deplacable a droite"""
        # on place manuellement un obstacle deplacable a droite du robot
        self.grille.grid[2][3] = "°"
        self.grille.grid[2][4] = "X"
        res = self.grille.right(1)
        self.assertEqual(res, False)


if __name__ == "__main__":

    unittest.main()
