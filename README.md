# AdmCo - TP n°2

## Objectif
L'objectif de ce projet est de créer un package _Package_robot_ permettant la gestion d'un robot sous forme de grille. 

## Réalisation
On créé dans le package _Package_robot_ une classe _Grid_. Cette clase génère une grille de "0" dont la taille est entrée en paramètre, et initialise un robot, représenté par un "1" à le position (0,0). La classe comporte des méthodes permettant d'afficher l'état de la grille, et de déplacer le robot dans toutes les directions.

## Limites
Après plusieur tests, on remarque que des grilles contenant plus de 10^10 cases mettent beacoup de temps à se générer. On veillera donc à ne pas créer de grille de taille trop importante.

## Environnement virtuel Python
On travail dans un environnement virtuel pour gérer les dépendances indépendament du reste du système. On indique les librairies à installer dans un fichier _requirements.txt_, et on les installe avec pip dans l'environnement virtuel. Pour mettre en place un environnement virtuel, on exécute les commandes suivantes :

    sudo apt-get install python3-venv

    python3 -m venv venv

    source ./venv/bin/activate

    pip install -r requirements.txt

On désactive l'environnement virtuel avec la commande :

    deactivate

## Test
Pour tester le bon fonctionnement du package _Package_robot_, on créé un package _Package_test_ contenant une classe _Test_grille_ réalisant plusieurs tests à l'aide du module _unittest_.
Pour lancer le programme de test en local, il faut tout d'abord installer le module _unittest_ :

    pip install unittest

On doit également initialiser le PYTHONPATH à la racine du répertoire du projet pour que le script de test ai accès au package _Package_robot_ : 

    export PYTHONPATH=$PYTHONPATH:'.'

On execute enfin le programme :

    python3 Package_test/test_tp2.py

En cas de succès, on obtient le résultat suivant :

    as@as-HP-Pavilion-Laptop:~/Documents/AdmCo/tp2_admco$ python3 Package_test/test_tp2.py 
    ........Erreur : taille de grille
    .Erreur : taille de grille
    ..
    ----------------------------------------------------------------------
    Ran 11 tests in 0.001s

    OK


## Création d'un paquet installable
Pour pouvoir utiliser le pacquet créé, il faut d'abord pouvoir l'importer. Pour cela, plusieurs solutions :

--> ajouter le chemin du package au PYTHONPATH 

    export PYTHONPATH=PYTHONPATH:'.'

--> créer une version déployable du paquet. 
Pour cela on créer un fichier _setup.py_ à la racine du projet contenant des informations sur le paquet à créer.

    from setuptools import setup

    setup(
        name="PackageRobot-ASibenaler-tag-v1",
        version="0.0.1",
        author="Arnaud Sibenaler",
        packages=["Package_robot"],
        description="Package permettant de simuler le déplacement d'un robot \
            sur une grille.",
        license="GNU GPLv3",
        python_requires=">=3.4",
    )

On génère ensuite une version deployable du paquet ():

    python3 setup.py bdist sdist bdist_wheel

On peut ensuite installer le paquet :

    pip install dist/PackageRobot_ASibenaler_tag_v1-0.0.1-py3-none-any.whl

--> déployer le paquet sur Test Pypi. 
On peut exporter l'archive générée

    pip install twine
    python3 setup.py bdist_wheel

    python3 -m twine upload --repository testpypi dist/*

On peut à présent installer le paquet depuis Test Pypi sur n'importe quelle machine :

    pip install -i ttps://test.pypi.org/simple/PackageRobot-ASibenaler-tag-v1

Pour installer la dernière version : 

    pip install -i https://test.pypi.org/simple/ PackageRobot-ASibenaler-tag-v4==0.0.1
    

## Intégration continue
Le projet étant enregistré sur Gitlab, on peut utiliser l'intégration continue proposée par le plateforme.
On créé un fichier _.gitlab-ci.yml_ contenant les actions à exécuter automatiquement à chaque push du code.
On installe les dépendances requises, indiquées dans un fichier _requirements.txt_. On vérifie ensuite le code avec la librairie _flake8_. On effectue les tests avec _pytest_, et on génère enfin les versions déployables du paquet.
En cas de succès, on a les résultats suivants :

![Capture d’écran du 2023-02-27 00-03-46](images/Capture d’écran du 2023-02-27 00-03-46.png)

## Tag v2.0
On ajoute à la classe _Grid_ la possibilité de générer un nombre aléatoire d'obstacles (compris entre la taille de la grille /15 et la taille de la grille /10). Pour cela on créé une méthode _init_obstacle_ remplaçant certaines cases aléatoires (à l'exception des cases autour de la case de départ) par le caractère "X". On empêche le déplacement du robot vers les cases contenant un "X".  
On modifie la classe test pour vérifier que des obstacles sont bien générés, et que le robot ne peut pas se déplacer sur une case contenant un obstacle.

## Tag v3.0
On ajoute à la classe _Grid_ la possibilité d'avoir plusieurs robots simultanément sur la grille.  
Pour cela, on modifie le constructeur de la classe pour qu'il prenne en paramètre le nombre de robots que l'on souhaite créer, en plus de la taille de la grille. Le nombre de robot ne peut pas être supérieur à min(hauteur, largeur)-1, et prend cette valeur si le nombre entré est supérieur.  
Les positions de départs de chaque robot sont aléatoires, et sont contenues dans deux listes.  
Chaque robot est représenté sur la grille par un nombre différent.
On modifie les méthodes de déplacement pour que l'on ait à préciser le numéro du robot que l'on souhaite déplecer en paramètre, et que les robots ne puissent pas se déplacer sur une case contenant déjà un robot.  
On modifie également la classe de test pour tester que l'on puisse bien générer plusieurs robots, que l'on puisse déplacer indépendament chaque robot, et que les collisions entres les robots fonctionnents.

## Tag v4.0
On ajoute à la classe _Grid_ la possibilité de générer en plus des obstacles déjà présents, des obstacles déplacables, positionnés aléatoirement et en même nombre. On fait attention à générer ces obstacles sur des cases qui ne sont pas déjà occupées par un robot ou un autre obstacle. Ces obstacles peuvent être poussés par un robot si ils ont une case libre dans la direction du mouvement.  
Ces obstacles sont générés dans un méthode _init_obstacle_deplacable_, que l'on appelle en initialisant la grille. On modifie les méthodes de déplacement pour prendre en compte ces obstacles et gérer leurs déplacements.
On test les collisions entre les robot et les obstacles déplacables dans le package de test. On test tout d'abord le déplacement d'un obstacle, poussé par un robot, puis la collision entre un robot et un obstacle deplacable bloqué par un autre obstacle, en plus des autres tests.  
/!\ On ne vérifie plus ici le code avec flake8 lors dans l'intégration continue. Le code est vérifie bien les critères de flake8 mais comporte certaines lignes de plus de 79 caractères.
