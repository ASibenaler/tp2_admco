from setuptools import setup

setup(
    name="PackageRobot-ASibenaler-tag-v4",
    version="0.0.1",
    author="Arnaud Sibenaler",
    packages=["Package_robot"],
    description="Package permettant de simuler le déplacement d'un robot \
        sur une grille (tag v4).",
    license="GNU GPLv3",
    python_requires=">=3.4",
)
